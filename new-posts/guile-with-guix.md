title: Setting up a GNU Guile project with GNU Guix
date: 2017-01-20 12:00
tags: guile, guix, autotools
---

Lisp, specifically Scheme, has captivated me for about two years now. My Scheme
implementation of choice is [GNU Guile][guile] because it is the official
extension language of the GNU project.  One issue I faced early with when trying
to develop for Guile was how to set up and organize my project. There doesn't
seem to be any single recommended way to set up a Guile project but several
projects do follow a similiar project structure I will describe below. At the
same time as I was learning Scheme, I discovered a very interesting project
called [GNU Guix][guix]. Guix is a functional package manager that is part of
the GNU project which can be installed on top of an existing GNU/Linux
distributions or used as part of GuixSD. As the type of package management might
suggest, Guix treats the build and installation of a package as a pure function
where the inputs are the dependencies of the project and the output is the
installed package. Guix is much more than just a package manager, it provides
tools for building operating systems, virtual machines, containers, and
more. One such feature called guix environments will be demonstrated below as a
useful tool for development.

## Simple project structure
The project template I created for new projects is based on several GNU Guile
projects I have examined. These projects follow the traditional GNU Build System
using the familiar commands `./configure && make && sudo make install` for
building and installing software. To help generate these files, I use the
collection of software known as autotools which include the software
[autoconf][autoconf] and [automake][automake]. Unfortunately, autotools can be
quite complex for developers with esoteric languages like m4 being used to
magically create and configure all the necessary build files for your
project. Good news for us, not much magic is needed for us to conjure
the build files for a Guile project.

```
.
├── bootstrap
├── configure.ac
├── COPYING
├── COPYING.LESSER
├── guile.am
├── guix.scm
├── m4
│   └── guile.m4
├── Makefile.am
├── module
│   └── skele.scm
├── pre-inst-env.in
├── README
├── skeleton.scm
└── test-env.in
```

Above is the directory structure of the project. `bootstrap` is a simple shell
script which a developer can regenerate all the GNU Build System
files. `configure.ac` is template file which autoconf uses to generate the
familiar `configure` script. `m4/guile.m4` is a recent copy of guile's m4
macros, not needed if developing with a recent version of guile (like
Guile-2.2).  `COPYING` and `COPYING.LESSER` are just the GPL and LGPL
licenses. `Makefile.am` and `guile.am` are automake files used to generate the
`Makefile.in` which `configure` will configure. `guix.scm` is a guix package
definition of our guile project. `skeleton.scm` and `module/skele.scm` are some
initial source code files, where `skeleton.scm` represents the Guile module
`(skeleton)` and `module/skele.scm` is the `(module skele)` module, change these
file and directory names to what you want to name your modules
as. `pre-inst-env.in` and `test-env.in` are shell scripts which set up
environment varibles to be able to use your code before installing it and during
testing.

```sh
#! /bin/sh

autoreconf -vif
```

This is the `bootstrap` script, it just calls autoreconf to generate the
`configure` script from `configure.ac` and `Makefile.in` file from
`Makefile.am`. The command will also generate a bunch of other files as
well. This script is only needed when building from a checkout of the project's
repository and does not need to be distributed in releases. Whenever you might
be having an issue with the configure script, doing `./bootstrap` may help by
regenerating the build files.

```
AC_INIT([guile-skeleton], [0.1])
AC_CONFIG_SRCDIR([skeleton.scm])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])
AM_INIT_AUTOMAKE([-Wall -Werror foreign])

GUILE_PKG([2.2 2.0])
GUILE_PROGS([2.0.11])

AC_CONFIG_FILES([Makefile])
AC_CONFIG_FILES([pre-inst-env], [chmod +x pre-inst-env])
AC_CONFIG_FILES([test-env], [chmod +x test-env])


AC_OUTPUT
```

Above is the `configure.ac` file used by [GNU
Autoconf](https://www.gnu.org/s/autoconf/) to generate the configure script. The
first line is the `AC_INIT` macro, the first argument is the package name and
the second argument is the version. There is a couple of other optional
arguments which you can learn more from
[here](https://www.gnu.org/savannah-checkouts/gnu/autoconf/manual/autoconf-2.69/html_node/Initializing-configure.html#Initializing-configure).
The `AC_CONFIG_SRCDIR` macro adds a check to `configure` for the existance of a
unique file in the source directory, useful as a safety check to make sure a
user is configuring the correct project. `AC_CONFIG_AUX_DIR` macro is where
auxilary builds tools are found, `build-aux` is the the most common directory,
we use this so we don't litter the source directory with build tools.  The next
macro, `AC_CONFIG_MACRO_DIR` is where additional macros can be found, we add
this to include the `m4/guile.m4` file. [GNU
Automake](https://www.gnu.org/s/automake/) options are part of the next macro,
`AM_INIT_AUTOMAKE`, where `-Wall` turns all warnings, `-Werror` turns those
warnings into errors, and finally `foreign` will turn the strictness to a
standard less than the GNU standard. More automake options can be found
[here](https://www.gnu.org/software/automake/manual/html_node/List-of-Automake-options.html#List-of-Automake-options).


```
include guile.am

moddir=$(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/ccache

SOURCES =                           \
  module/skele.scm                  \
  skeleton.scm

TESTS =	                                       \
  tests/node.scm

TEST_EXTENSIONS = .scm

SCM_LOG_COMPILER = $(top_builddir)/test-env $(GUILE)
AM_SCM_LOG_FLAGS = --no-auto-compile -L "$(top_srcdir)"

EXTRA_DIST += README.md                        \
              pre-inst-env.in                  \
              test-env.in                      \
              $(TESTS)
```

[guix]: https://www.gnu.org/software/guix/ "GNU Guix functional package management"
[guile]: https://www.gnu.org/software/guile/ "GNU extension language"
[autoconf]: https://www.gnu.org/software/autoconf/autoconf.html
[automake]: https://www.gnu.org/software/automake/
